from flask import Flask, request
from twilio.twiml.messaging_response import MessagingResponse
from twilio.rest import Client
import threading
import time
import vst
from vst import *
from collections import deque

app = Flask(__name__)

MAX_LENGTH = 5
queue=deque(maxlen = MAX_LENGTH)

machine_in_use = False

@app.route("/msg_received", methods=['GET', 'POST'])
def process_messages():
	global machine_in_use
    # Start our response
        account_sid = "AC0052ea6a703857c046281435d9e44ec9"
        auth_token = "164118cb1d2193023770de271e6c24ba"

        client = Client(account_sid, auth_token)
	resp = MessagingResponse()

	number = request.form['From']
 
	message_body = request.form['Body']
	if message_body == 'Start':
            if queue.count(number) > 0:
                if queue[0] == number and machine_in_use == True:
                    client.messages.create(to=number,from_="+17082953685",body="You are using the machine.")
                    print ("yautm")
                    return ("using the machine")
                elif queue[0] == number and machine_in_use == False:
                    pass
                    
                else:
                    client.messages.create(to=number,from_="+17082953685",body="You are already in line")
                    
                    return ("already in line")
            else:
                if len(queue) == 5:
                    client.messages.create(to=number,from_="+17082953685",body="Please come back later!")

                    return ("come back later")
             	queue.append(number)
             
            
            if (queue[0]== number):
		#resp.message(str( read_vibration()))
		client.messages.create(to=number,from_="+17082953685",body="Thank you! we will text you when your laundry is done");
               	machine_in_use = True

                while True:
			if(vst.sleepTimer()):
				break
		client.messages.create(to=number,from_="+17082953685",body="Your laundry is done. Please come Pick it up.");
		queue.popleft()
		if (queue[0] > 0):
			client.messages.create(to=queue[0], from_="+17082953685", body="Thank you for waiting. It is your turn to use the laundry machine. Please text Start again when you are beginning to do your laundry")
		machine_in_use = False
		return ("pick it up")
		
	    else:
                client.messages.create(to=number,from_="+17082953685",body="You have been added to the waitlist. We will text you when it is your turn to use the laundry machine.")
                
                
#		if not loop():
#			resp.message("Your laundry is done. Please come pick it up!") 	
	else :
		client.messages.create(to=number,from_="+17082953685",body="Please try again!");
		
	return str(resp)


if __name__ == "__main__":
    app.run(debug=True, threaded=True)

