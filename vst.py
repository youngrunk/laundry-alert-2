import time

import RPi.GPIO as GPIO

#vibration sensor setup
GPIO.setmode(GPIO.BCM)

# freely chosen SPI pins
SPICLK = 16  # BOARD 36
SPIMISO = 19  # BOARD 35
SPIMOSI = 20  # BOARD 38
SPICS = 25  # BOARD 22
 
# set up the SPI interface pins
GPIO.setup([SPIMOSI, SPICLK, SPICS], GPIO.OUT)
GPIO.setup(SPIMISO, GPIO.IN)
 
# 10k trim pot connected to adc #0
vibration_adc = 0;

 
# read SPI data from MCP3008 chip, 8 possible adc's (0 thru 7)
def readadc(adcnum, clockpin, mosipin, misopin, cspin):
        if ((adcnum > 7) or (adcnum < 0)):
                return -1
        GPIO.output(cspin, True)
 
        GPIO.output(clockpin, False)  # start clock low
        GPIO.output(cspin, False)     # bring CS low
 
        commandout = adcnum
        commandout |= 0x18  # start bit + single-ended bit
        commandout <<= 3    # we only need to send 5 bits here
        for i in range(5):
                if (commandout & 0x80):
                        GPIO.output(mosipin, True)
                else:
                        GPIO.output(mosipin, False)
                commandout <<= 1
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
 
        adcout = 0
        # read in one empty bit, one null bit and 10 ADC bits
        for i in range(12):
                GPIO.output(clockpin, True)
                GPIO.output(clockpin, False)
                adcout <<= 1
                if (GPIO.input(misopin)):
                        adcout |= 0x1
 
        GPIO.output(cspin, True)
        
        adcout >>= 1       # first bit is 'null' so drop it
        return adcout
 
def read_vibration():
    vibration_value = readadc(vibration_adc, SPICLK, SPIMOSI, SPIMISO, SPICS)
#    print(vibration_value)
    return vibration_value

def loop():
	while read_vibration()>100:
		return false
 
#Timer for the whole wash of the washing machine
def timer():
	while read_vibration > 1000:
		n = 0
		if n < 1620:
			time.sleep(1)
			n = n + 1
	return n

#Timer for the sleep function of the vibration sensor
def sleepTimer():
	counter = 0
	vibration = 500
	#print "initializing sleepTimer"

	# while loop only breaks when:
	#	- vibration>=100 (laundry is in use) we exit with False
	# 	- if we reach 20 seconds (counter=40) we exit with True
	stupid = 0
        while stupid < 100:
		currentTime = time.time()
		stupid += 1
		# delta timing, check vibrations every half second
		while time.time() - currentTime < 0.502:
			vibration = read_vibration()
                #print("VIBRATION:",vibration)
		if vibration < 100:
			counter = counter + 1
			#print(counter)
			if counter >= 40:
				#print("should stop laundry")
				return True
		else:
			return False

#		t = Timer(40.0, read_vibration())
#		if t.start() < 100:
#			return True
if __name__ == "__main__":
    import time
#    while True:
#        vibration_value = readadc(vibration_adc, SPICLK, SPIMOSI, SPIMISO, SPICS)
#        print("vibration_value:", vibration_value)
#        print("normalized: ", round(vibration_value / 1024.0, 2))
#	time.sleep(0.5)

#app = Flask(__name__)

#@app.route("/msg_recieved", methods=['GET', 'POST'])
#def sms_ahoy_reply():
#	"""Respond to incoming messages with a friendly SMS."""
#	#start our response
#	resp = MessagingResponse()
#	#sender's number 
#	number = request.form['From']
#	message_body = request.form['Body']
#
#	#Add a message 
#	if message_body == 'Start':
#		resp.message("Thanks you for using LaundryAlert! We will message you when your laundry is done.")
#		if read_vibration() < 100 && sleepTimer() >= 40:
#				resp.message("Your laundry is done. Please go pick it up as soon as possible.)
#
#	#Time remaining 
#	if message_body == 'Time'
#		a = 1620 - timer()
#		resp.message("The time remaining is: " a)
#	
#	#Any other Command
#	else:
#		resp.massage("Please try again!")
#	return str(resp)
#
#if __name__ == "__main__":
#	app.run(debug=True)
